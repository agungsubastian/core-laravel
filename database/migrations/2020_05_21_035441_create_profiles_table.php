<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->bigIncrements('profiles_id')->unique();
            $table->unsignedBigInteger('profiles_users_id');
            $table->string('profiles_first_name');
            $table->string('profiles_last_name');
            $table->string('profiles_born_date');
            $table->string('profiles_born_place');
            $table->enum('profiles_gender', ['Laki-Laki','Perempuan']);
            $table->string('profiles_address');
            $table->string('profiles_phone');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
